#pragma once

#include <glad/glad.h>

#include <algorithm>
#include <sstream>

#include <ft2build.h>
#include <freetype/freetype.h>

#include "TexturePacker.h"

/// Holds all state information relevant to a character as loaded using FreeType
struct Character {
	glm::ivec2 origin;
	glm::ivec2 size;    // Size of glyph
	glm::ivec2 bearing;  // Offset from baseline to left/top of glyph
	GLuint Advance;    // Horizontal offset to advance to next glyph
};

class SortCharacterDescending
{
public:
	SortCharacterDescending(std::vector<Character>& characters) : characters(characters) { }
	bool operator () (int i1, int i2)
	{
		Character& c1 = characters[i1];
		Character& c2 = characters[i2];
		return (c1.size.x * c1.size.y) > (c2.size.x * c2.size.y);
	}
private:
	std::vector<Character>& characters;
};

struct Font
{
	Font() : texturePacker(glm::ivec2(64, 64)) { }

	Font(const Font&) = delete;
	Font& operator=(const Font&) = delete;

	Font(const std::string& fontPath, int height)
		: Font()
	{
		this->loadCharacters(fontPath, height);
	}

	~Font() { }

	/*! The OpenGL texture ID for the font atlas. */
	GLuint textureID;

	/*! The characters which were loaded during loadCharacters. */
	std::vector<Character> characters;

	/*! The texture packer we used to pack the character textures. */
	TexturePacker texturePacker;

	void loadCharacters(const std::string& fontPath, int height)
	{
		FT_Library ft;
		if (FT_Init_FreeType(&ft)) {
			fprintf(stderr, "ERROR::FREETYPE: Could not init FreeType Library");
			return;
		}

		FT_Face face;
		if (FT_New_Face(ft, fontPath.c_str(), 0, &face)) {
			fprintf(stderr, "ERROR::FREETYPE: Failed to load font %s\n", fontPath.c_str());
			return;
		}

		characters.clear();

		FT_Set_Pixel_Sizes(face, 0, height);

		for (int i = 32; i < 128; i++)
		{
			char c = (char)i;
			// Load character glyph
			if (FT_Load_Char(face, c, FT_LOAD_RENDER))
			{
				fprintf(stderr, "ERROR::FREETYTPE: Failed to load Glyph %c", c);
				continue;
			}

			// Now store character for later use
			Character character = {
				glm::ivec2(0,0),
				glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
				glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
				face->glyph->advance.x,
			};
			characters.push_back(character);
		}

		SortCharacterDescending descending = SortCharacterDescending(characters);
		std::vector<int> sortedCharacters(characters.size());
		for (size_t i = 0; i < sortedCharacters.size(); i++) {
			sortedCharacters[i] = i;
		}
		std::sort(sortedCharacters.begin(), sortedCharacters.end(), descending);

		int i = 0;
		for (size_t i = 0; i < sortedCharacters.size(); i++)
		{
			int charIndex = sortedCharacters[i];
			Character& character = characters[charIndex];
			if (FT_Load_Char(face, charIndex, FT_LOAD_RENDER))
			{
				fprintf(stderr, "ERROR::FREETYTPE: Failed to load Glyph %c", charIndex);
				continue;
			}

			if (isprint(charIndex))
			{
				packCharacter(character, face->glyph->bitmap);

				if (character.size.x > 0 && character.size.y > 0) {
					int basenameIndex = fontPath.find_last_of('/');
					std::string basename = fontPath.substr(basenameIndex + 1);
					std::stringstream sstream;
					sstream << "data/" << basename << "_" << height << "_" << i << ".bmp";
					saveAtlasToFile(sstream.str());
				}
			}
		}

		glm::ivec2 textureSize = texturePacker.getTextureSize();
		const unsigned char* buffer = texturePacker.getBuffer();

		FT_Done_Face(face);
		FT_Done_FreeType(ft);

		// Give OpenGL the data
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glGenTextures(1, &textureID);
		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, textureSize.x, textureSize.y, 0, GL_RED, GL_UNSIGNED_BYTE, buffer);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}

	void saveAtlasToFile(const std::string& file) const
	{
		glm::ivec2 textureSize = texturePacker.getTextureSize();
		const unsigned char* buffer = texturePacker.getBuffer();
	}

	Character getCharacter(unsigned int i) const
	{
		if (i < characters.size()) {
			return characters[i];
		}
		return Character{ glm::ivec2(0,0), glm::ivec2(0,0), glm::ivec2(0,0), 0 };
	}

	//Texture getTexture() const
	//{
	//	Texture texture;
	//	texture.type = TextureType_diffuse;
	//	texture.id = textureID;
	//	return texture;
	//}

	glm::ivec2 getTextureSize() const
	{
		return texturePacker.getTextureSize();
	}

	/*!
	 * \brief Packs a single character into the font atlas.
	 * \param character The character to pack. Everything must be filled except origin. The origin field
	 *		is filled with the top-left position of the character within the font atlas.
	 * \param bitmap The actual glyph data which we pack into the atlas.
	 */
	void packCharacter(Character& character, const FT_Bitmap& bitmap)
	{
		if (character.size.x == 0 || character.size.y == 0) {
			return;
		}

		glm::ivec2 origin = texturePacker.packTexture(bitmap.buffer, glm::ivec2(bitmap.pitch, bitmap.rows));
		character.origin = origin;
	}
};